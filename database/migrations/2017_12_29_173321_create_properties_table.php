<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('permalink');
            $table->string('title');
            $table->string('description');
            $table->text('images');
            $table->string('lat');
            $table->string('lng');
            $table->string('price');
            $table->string('baths');
            $table->string('beds');
            $table->string('parking');
            $table->string('lang');
            $table->integer('status_id')->unsigned()->index();
            $table->integer('location_id')->unsigned()->index();
            $table->integer('type_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
