<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = "features";
    protected $fillable = ['title', 'lang'];
    public $timestamps = false;

    public function properties(){
        return $this->belongsToMany('App\Properties', 'property_has_features');
    }
}
