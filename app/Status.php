<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";
    protected $fillable = [ 'title', 'lang'];
    public $timestamps = false;

    public function property(){
        return $this->hasMany('App\Property');
    }
}
