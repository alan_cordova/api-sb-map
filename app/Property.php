<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = "properties";

    protected $fillable = [
        'permalink',
        'title',
        'description',
        'images',
        'price',
        'price_type',
        'baths',
        'beds',
        'parking',
        'lang',
        'property_id',
        'status_id',
        'location_id',
        'type_id',
        'lat',
        'lng',
        'last_update',
    ];

    public function location(){
        return $this->belongsTo('Location');
    }

    public function type(){
        return $this->belongsTo('Type');
    }

    public function status(){
        return $this->belongsTo('Status');
    }

    public function features(){
        return $this->belongsToMany('App\Feature', 'property_has_features');
    }

    /** ***/

    

    public function getImagesMLS($gallery){
        $images = [];
        $images['image0'] = $gallery;

        return $images;
    }

    public function getFeaturesIds($featuresArray, $lang){
        $features = [];
        foreach ($featuresArray as $feature) {
            if ($lang == 'en') {
                switch ($feature['slug']) {
                    // Features en Ingles
                    case 'handicap-facilities':
                        $features[] = 1;
                        break;
                    case 'air-conditioning':
                        $features[] = 3;
                        break;
                    case 'pool':
                        $features[] = 5;
                        break;
                    case 'pet-friendly':
                        $features[] = 7;
                        break;
                    case 'furnished':
                        $features[] = 9;
                        break;
                    case 'bbq-area':
                        $features[] = 11;
                        break;
                    case 'childrens-playground':
                        $features[] = 13;
                        break;
                    case 'grill-top':
                        $features[] = 15;
                        break;
                    case 'balconyterrace':
                        $features[] = 17;
                        break;
                    case 'hot-tub':
                        $features[] = 21;
                        break;
                    case 'pool-bar':
                        $features[] = 23;
                        break;
                    case 'safe-box':
                        $features[] = 25;
                        break;
                    case 'gas-hot-water':
                        $features[] = 27;
                        break;
                    case 'electric-hot-water':
                        $features[] = 29;
                        break;
                    case 'tennis-court':
                        $features[] = 31;
                        break;
                    case 'washerdryer':
                        $features[] = 33;
                        break;
                    case 'fence':
                        $features[] = 35;
                        break;
                    case 'lawn':
                        $features[] = 37;
                        break;
                    case 'children-pool':
                        $features[] = 39;
                        break;
                    case 'beach-club':
                        $features[] = 41;
                        break;
                    case 'kitchen':
                        $features[] = 43;
                        break;
                    case 'fully-equiped-kitchen':
                        $features[] = 46;
                        break;
                    case 'freezer':
                        $features[] = 47;
                        break;
                    case 'water-softener':
                        $features[] = 49;
                        break;
                    case 'elevator':
                        $features[] = 51;
                        break;
                    case 'parking':
                        $features[] = 53;
                        break;
                    case 'gas-stove':
                        $features[] = 55;
                        break;
                    case 'financing':
                        $features[] = 57;
                        break;
                    case 'beach-front':
                        $features[] = 59;
                        break;
                    case 'gym':
                        $features[] = 61;
                        break;
                    case 'oven':
                        $features[] = 63;
                        break;
                    case 'exterior-lighting':
                        $features[] = 65;
                        break;
                    case 'internet':
                        $features[] = 67;
                        break;
                    case 'jacuzzi':
                        $features[] = 69;
                        break;
                    case 'jacuzzi-tub':
                        $features[] = 71;
                        break;
                    case 'garden':
                        $features[] = 73;
                        break;
                    case 'dishwasher':
                        $features[] = 75;
                        break;
                    case 'maintenance':
                        $features[] = 77;
                        break;
                    case 'microwave':
                        $features[] = 79;
                        break;
                    case 'boat-slip':
                        $features[] = 81;
                        break;
                    case 'stories':
                        $features[] = 83;
                        break;
                    case 'landscaping':
                        $features[] = 85;
                        break;
                    case 'covered-patio':
                        $features[] = 87;
                        break;
                    case 'reception':
                        $features[] = 89;
                        break;
                    case 'refrigerator':
                        $features[] = 91;
                        break;
                    case 'living-room':
                        $features[] = 93;
                        break;
                    case 'children-allowed':
                        $features[] = 95;
                        break;
                    case 'security':
                        $features[] = 97;
                        break;
                    case 'maid-services':
                        $features[] = 99;
                        break;
                    case 'sprinkler-system':
                        $features[] = 101;
                        break;
                    case 'telephone':
                        $features[] = 103;
                        break;
                    case 'tv-set':
                        $features[] = 105;
                        break;
                    case 'cable-tv':
                        $features[] = 107;
                        break;
                    case 'satellite-tv':
                        $features[] = 109;
                        break;
                    case 'terrace':
                        $features[] = 19;
                        break;
                    case 'ceiling-fan':
                        $features[] = 111;
                        break;
                    case 'ocean-view':
                        $features[] = 113;
                        break;
                    case 'river-view':
                        $features[] = 115;
                        break;
                    case 'street-view':
                        $features[] = 117;
                        break;
                    case 'mountain-view':
                        $features[] = 119;
                        break;
                    case 'panoramic-view':
                        $features[] = 121;
                        break;
                    case 'views':
                        $features[] = 123;
                        break;
                }
            }else if ($lang == 'es') {
                switch ($feature['slug']) {
                    // Features en Español
                    case 'acceso-a-discapacitados':
                        $features[] = 2;
                        break;
                    case 'aire-acondicionado':
                        $features[] = 4;
                        break;
                    case 'alberca':
                        $features[] = 6;
                        break;
                    case 'amigable-para-las-mascotas':
                        $features[] = 8;
                        break;
                    case 'amueblado':
                        $features[] = 10;
                        break;
                    case 'area-comun-con-asador':
                        $features[] = 12;
                        break;
                    case 'area-de-juegos-para-ninos':
                        $features[] = 14;
                        break;
                    case 'asador':
                        $features[] = 16;
                        break;
                    case 'balconterraza':
                        $features[] = 18;
                        break;
                    case 'banera-de-hidromasaje':
                        $features[] = 22;
                        break;
                    case 'bar-en-alberca':
                        $features[] = 24;
                        break;
                    case 'caja-fuerte':
                        $features[] = 26;
                        break;
                    case 'calentador-de-gas':
                        $features[] = 28;
                        break;
                    case 'calentador-electrico':
                        $features[] = 30;
                        break;
                    case 'cancha-de-tenis':
                        $features[] = 32;
                        break;
                    case 'centro-de-lavado':
                        $features[] = 34;
                        break;
                    case 'cerca':
                        $features[] = 36;
                        break;
                    case 'cesped':
                        $features[] = 38;
                        break;
                    case 'chapoteadero':
                        $features[] = 40;
                        break;
                    case 'club-de-playa':
                        $features[] = 42;
                        break;
                    case 'cocina':
                        $features[] = 44;
                        break;
                    case 'cocina-totalmente-equipada':
                        $features[] = 46;
                        break;
                    case 'congelador':
                        $features[] = 48;
                        break;
                    case 'descalcificador':
                        $features[] = 50;
                        break;
                    case 'elevador':
                        $features[] = 52;
                        break;
                    case 'estacionamiento':
                        $features[] = 54;
                        break;
                    case 'estufa-de-gas':
                        $features[] = 56;
                        break;
                    case 'financiamiento':
                        $features[] = 58;
                        break;
                    case 'frente-a-la-playa':
                        $features[] = 60;
                        break;
                    case 'gimnasio':
                        $features[] = 62;
                        break;
                    case 'horno':
                        $features[] = 64;
                        break;
                    case 'iluminacion-exterior':
                        $features[] = 66;
                        break;
                    case 'internet-es':
                        $features[] = 68;
                        break;
                    case 'jacuzzi-es':
                        $features[] = 70;
                        break;
                    case 'jacuzzi-con-hidromasaje':
                        $features[] = 72;
                        break;
                    case 'jardin':
                        $features[] = 74;
                        break;
                    case 'lavavajillas':
                        $features[] = 76;
                        break;
                    case 'mantenimiento':
                        $features[] = 78;
                        break;
                    case 'microondas':
                        $features[] = 80;
                        break;
                    case 'muelle':
                        $features[] = 82;
                        break;
                    case 'niveles':
                        $features[] = 84;
                        break;
                    case 'paisajismo':
                        $features[] = 86;
                        break;
                    case 'patio-techado':
                        $features[] = 88;
                        break;
                    case 'recepcion':
                        $features[] = 90;
                        break;
                    case 'refrigerador':
                        $features[] = 92;
                        break;
                    case 'sala':
                        $features[] = 94;
                        break;
                    case 'se-permiten-ninos':
                        $features[] = 96;
                        break;
                    case 'seguridad':
                        $features[] = 98;
                        break;
                    case 'servicio-de-limpieza':
                        $features[] = 100;
                        break;
                    case 'sistema-de-riego':
                        $features[] = 102;
                        break;
                    case 'telefono':
                        $features[] = 104;
                        break;
                    case 'television':
                        $features[] = 106;
                        break;
                    case 'television-por-cable':
                        $features[] = 108;
                        break;
                    case 'television-satelital':
                        $features[] = 110;
                        break;
                    case 'terraza':
                        $features[] = 20;
                        break;
                    case 'ventilador-de-techo':
                        $features[] = 112;
                        break;
                    case 'vista-al-mar':
                        $features[] = 114;
                        break;
                    case 'vista-al-rio':
                        $features[] = 116;
                        break;
                    case 'vista-de-calle':
                        $features[] = 118;
                        break;
                    case 'vista-de-montana':
                        $features[] = 120;
                        break;
                    case 'vista-panoramica':
                        $features[] = 122;
                        break;
                    case 'vistas':
                        $features[] = 124;
                        break;
                }
            }
        }

        return $features;
    }
    

    public function getFeaturesIdsMLS($featuresArray, $lang){
        $features = [];
        foreach ($featuresArray as $feature) {
            if ($lang == 'en') {
                switch ($feature) {
                    // Features en Ingles
                    case 'handicap-facilities':
                        $features[] = 1;
                        break;
                    case 'air-conditioning':
                        $features[] = 3;
                        break;
                    case 'pool':
                        $features[] = 5;
                        break;
                    case 'pet-friendly':
                        $features[] = 7;
                        break;
                    case 'furnished':
                        $features[] = 9;
                        break;
                    case 'bbq-area':
                        $features[] = 11;
                        break;
                    case 'barbecue':
                        $features[] = 11;
                        break;
                    case 'childrens-playground':
                        $features[] = 13;
                        break;
                    case 'grill-top':
                        $features[] = 15;
                        break;
                    case 'fire-pit':
                        $features[] = 15;
                        break;
                    case 'balconyterrace':
                        $features[] = 17;
                        break;
                    case 'balcony':
                        $features[] = 17;
                        break;
                    case 'hot-tub':
                        $features[] = 21;
                        break;
                    case 'pool-bar':
                        $features[] = 23;
                        break;
                    case 'safe-box':
                        $features[] = 25;
                        break;
                    case 'gas-hot-water':
                        $features[] = 27;
                        break;
                    case 'electric-hot-water':
                        $features[] = 29;
                        break;
                    case 'tennis-court':
                        $features[] = 31;
                        break;
                    case 'washerdryer':
                        $features[] = 33;
                        break;
                    case 'clothes-washer':
                        $features[] = 33;
                        break;
                    case 'fence':
                        $features[] = 35;
                        break;
                    case 'lawn':
                        $features[] = 37;
                        break;
                    case 'children-pool':
                        $features[] = 39;
                        break;
                    case 'beach-club':
                        $features[] = 41;
                        break;
                    case 'kitchen':
                        $features[] = 43;
                        break;
                    case 'fully-equiped-kitchen':
                        $features[] = 46;
                        break;
                    case 'freezer':
                        $features[] = 47;
                        break;
                    case 'water-softener':
                        $features[] = 49;
                        break;
                    case 'water-purification':
                        $features[] = 49;
                        break;
                    case 'water-feature':
                        $features[] = 49;
                        break;
                    case 'elevator':
                        $features[] = 51;
                        break;
                    case 'parking':
                        $features[] = 53;
                        break;
                    case 'gas-stove':
                        $features[] = 55;
                        break;
                    case 'financing':
                        $features[] = 57;
                        break;
                    case 'beach-front':
                        $features[] = 59;
                        break;
                    case 'beachfront':
                        $features[] = 59;
                        break;
                    case 'gym':
                        $features[] = 61;
                        break;
                    case 'oven':
                        $features[] = 63;
                        break;
                    case 'exterior-lighting':
                        $features[] = 65;
                        break;
                    case 'internet':
                        $features[] = 67;
                        break;
                    case 'jacuzzi':
                        $features[] = 69;
                        break;
                    case 'jacuzzi-tub':
                        $features[] = 71;
                        break;
                    case 'garden':
                        $features[] = 73;
                        break;
                    case 'dishwasher':
                        $features[] = 75;
                        break;
                    case 'maintenance':
                        $features[] = 77;
                        break;
                    case 'microwave':
                        $features[] = 79;
                        break;
                    case 'boat-slip':
                        $features[] = 81;
                        break;
                    case 'deck':
                        $features[] = 81;
                        break;
                    case 'stories':
                        $features[] = 83;
                        break;
                    case 'landscaping':
                        $features[] = 85;
                        break;
                    case 'covered-patio':
                        $features[] = 87;
                        break;
                    case 'reception':
                        $features[] = 89;
                        break;
                    case 'refrigerator':
                        $features[] = 91;
                        break;
                    case 'living-room':
                        $features[] = 93;
                        break;
                    case 'sitting-area':
                        $features[] = 93;
                        break;
                    case 'children-allowed':
                        $features[] = 95;
                        break;
                    case 'security':
                        $features[] = 97;
                        break;
                    case '24-hours-security':
                        $features[] = 97;
                        break;
                    case 'security-system':
                        $features[] = 97;
                        break;
                    case 'security-guard':
                        $features[] = 97;
                        break;
                    case 'maid-services':
                        $features[] = 99;
                        break;
                    case 'sprinkler-system':
                        $features[] = 101;
                        break;
                    case 'telephone':
                        $features[] = 103;
                        break;
                    case 'tv-set':
                        $features[] = 105;
                        break;
                    case 'cable-tv':
                        $features[] = 107;
                        break;
                    case 'satellite-tv':
                        $features[] = 109;
                        break;
                    case 'terrace':
                        $features[] = 19;
                        break;
                    case 'roof-top-terrace':
                        $features[] = 19;
                        break;
                    case 'ceiling-fan':
                        $features[] = 111;
                        break;
                    case 'ceiling-fans':
                        $features[] = 111;
                        break;
                    case 'ocean-view':
                        $features[] = 113;
                        break;
                    case 'oceanfront':
                        $features[] = 113;
                        break;
                    case 'river-view':
                        $features[] = 115;
                        break;
                    case 'street-view':
                        $features[] = 117;
                        break;
                    case 'mountain-view':
                        $features[] = 119;
                        break;
                    case 'panoramic-view':
                        $features[] = 121;
                        break;
                    case 'views':
                        $features[] = 123;
                        break;
                    case 'fountain':
                        $features[] = 125;
                        break;
                    case 'maids-quarters':
                        $features[] = 127;
                        break;
                    case 'patio':
                        $features[] = 129;
                        break;
                    case 'pool-heater':
                        $features[] = 131;
                        break;
                    case 'storage-area':
                        $features[] = 133;
                        break;
                    case 'beach-access':
                        $features[] = 135;
                        break;
                    case 'bar':
                        $features[] = 137;
                        break;
                    case 'pergola':
                        $features[] = 139;
                        break;
                    case 'outdoor-shower':
                        $features[] = 141;
                        break;
                    case 'pet-area':
                        $features[] = 143;
                        break;
                    case 'wine-cooler':
                        $features[] = 145;
                        break;
                    case 'palapa':
                        $features[] = 147;
                        break;
                    case 'wine-cellar':
                        $features[] = 148;
                        break;
                }
            }else if ($lang == 'es') {
                switch ($feature) {
                    // Features en Español
                    case 'acceso-a-discapacitados':
                        $features[] = 2;
                        break;
                    case 'aire-acondicionado':
                        $features[] = 4;
                        break;
                    case 'alberca':
                        $features[] = 6;
                        break;
                    case 'amigable-para-las-mascotas':
                        $features[] = 8;
                        break;
                    case 'amueblado':
                        $features[] = 10;
                        break;
                    case 'area-comun-con-asador':
                        $features[] = 12;
                        break;
                    case 'barbecue':
                        $features[] = 12;
                        break;
                    case 'area-de-juegos-para-ninos':
                        $features[] = 14;
                        break;
                    case 'asador':
                        $features[] = 16;
                        break;
                    case 'fire-pit':
                        $features[] = 16;
                        break;
                    case 'balconterraza':
                        $features[] = 18;
                        break;
                    case 'balcony':
                        $features[] = 18;
                        break;
                    case 'banera-de-hidromasaje':
                        $features[] = 22;
                        break;
                    case 'bar-en-alberca':
                        $features[] = 24;
                        break;
                    case 'caja-fuerte':
                        $features[] = 26;
                        break;
                    case 'calentador-de-gas':
                        $features[] = 28;
                        break;
                    case 'calentador-electrico':
                        $features[] = 30;
                        break;
                    case 'cancha-de-tenis':
                        $features[] = 32;
                        break;
                    case 'centro-de-lavado':
                        $features[] = 34;
                        break;
                    case 'clothes-washer':
                        $features[] = 34;
                        break;
                    case 'cerca':
                        $features[] = 36;
                        break;
                    case 'cesped':
                        $features[] = 38;
                        break;
                    case 'chapoteadero':
                        $features[] = 40;
                        break;
                    case 'club-de-playa':
                        $features[] = 42;
                        break;
                    case 'cocina':
                        $features[] = 44;
                        break;
                    case 'cocina-totalmente-equipada':
                        $features[] = 46;
                        break;
                    case 'congelador':
                        $features[] = 48;
                        break;
                    case 'descalcificador':
                        $features[] = 50;
                        break;
                    case 'water-purification':
                        $features[] = 50;
                        break;
                    case 'water-feature':
                        $features[] = 50;
                        break;
                    case 'elevador':
                        $features[] = 52;
                        break;
                    case 'estacionamiento':
                        $features[] = 54;
                        break;
                    case 'estufa-de-gas':
                        $features[] = 56;
                        break;
                    case 'financiamiento':
                        $features[] = 58;
                        break;
                    case 'frente-a-la-playa':
                        $features[] = 60;
                        break;
                    case 'beachfront':
                        $features[] = 60;
                        break;
                    case 'gimnasio':
                        $features[] = 62;
                        break;
                    case 'horno':
                        $features[] = 64;
                        break;
                    case 'iluminacion-exterior':
                        $features[] = 66;
                        break;
                    case 'internet-es':
                        $features[] = 68;
                        break;
                    case 'jacuzzi-es':
                        $features[] = 70;
                        break;
                    case 'jacuzzi-con-hidromasaje':
                        $features[] = 72;
                        break;
                    case 'jardin':
                        $features[] = 74;
                        break;
                    case 'lavavajillas':
                        $features[] = 76;
                        break;
                    case 'mantenimiento':
                        $features[] = 78;
                        break;
                    case 'microondas':
                        $features[] = 80;
                        break;
                    case 'muelle':
                        $features[] = 82;
                        break;
                    case 'deck':
                        $features[] = 82;
                        break;
                    case 'niveles':
                        $features[] = 84;
                        break;
                    case 'paisajismo':
                        $features[] = 86;
                        break;
                    case 'patio-techado':
                        $features[] = 88;
                        break;
                    case 'recepcion':
                        $features[] = 90;
                        break;
                    case 'refrigerador':
                        $features[] = 92;
                        break;
                    case 'sala':
                        $features[] = 94;
                        break;
                    case 'sitting-area':
                        $features[] = 94;
                        break;
                    case 'se-permiten-ninos':
                        $features[] = 96;
                        break;
                    case 'seguridad':
                        $features[] = 98;
                        break;
                    case '24-hours-security':
                        $features[] = 98;
                        break;
                    case 'security-system':
                        $features[] = 98;
                        break;
                    case 'security-guard':
                        $features[] = 98;
                        break;
                    case 'servicio-de-limpieza':
                        $features[] = 100;
                        break;
                    case 'sistema-de-riego':
                        $features[] = 102;
                        break;
                    case 'telefono':
                        $features[] = 104;
                        break;
                    case 'television':
                        $features[] = 106;
                        break;
                    case 'television-por-cable':
                        $features[] = 108;
                        break;
                    case 'television-satelital':
                        $features[] = 110;
                        break;
                    case 'terraza':
                        $features[] = 20;
                        break;
                    case 'roof-top-terrace':
                        $features[] = 20;
                        break;
                    case 'ventilador-de-techo':
                        $features[] = 112;
                        break;
                    case 'ceiling-fans':
                        $features[] = 112;
                        break;
                    case 'vista-al-mar':
                        $features[] = 114;
                        break;
                    case 'oceanfront':
                        $features[] = 114;
                        break;
                    case 'vista-al-rio':
                        $features[] = 116;
                        break;
                    case 'vista-de-calle':
                        $features[] = 118;
                        break;
                    case 'vista-de-montana':
                        $features[] = 120;
                        break;
                    case 'vista-panoramica':
                        $features[] = 122;
                        break;
                    case 'vistas':
                        $features[] = 124;
                        break;
                    case 'fuente':
                        $features[] = 126;
                        break;
                    case 'maids-quarters':
                        $features[] = 128;
                        break;
                    case 'patio':
                        $features[] = 130;
                        break;
                    case 'pool-heater':
                        $features[] = 132;
                        break;
                    case 'storage-area':
                        $features[] = 134;
                        break;
                    case 'beach-access':
                        $features[] = 136;
                        break;
                    case 'bar':
                        $features[] = 138;
                        break;
                    case 'pergola':
                        $features[] = 140;
                        break;
                    case 'outdoor-shower':
                        $features[] = 142;
                        break;
                    case 'pet-area':
                        $features[] = 144;
                        break;
                    case 'wine-cooler':
                        $features[] = 146;
                        break;
                    case 'palapa':
                        $features[] = 148;
                        break;
                    case 'wine-cellar':
                        $features[] = 150;
                        break;
                }
            }
        }

        return $features;
    }


    

    

    
}
