<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use App\Property;
use App\Type;
use App\Location;
use App\Feature;

class propertiesController extends Controller
{

    public function storeProperties( Request $request ){ 

        try {
        
            if(!$request->properties){ return [];}

            foreach($request->properties as $k => $v){

                $property = Property::where('property_id', $v['property_id'])->first();
                if(!$property){
                    $property = new Property;
                    $property->property_id = $v['property_id'];
                }

                $property->lang = $v['lang'];

                $this->savePropertyData($property, $v);

                if(isset($v['is_mls'])){
                    $property_es = Property::where('property_id', $v['property_id'])->where('lang', 'es')->first();
                    if(!$property_es){
                        $property_es = new Property;
                        $property_es->lang = 'es';
                        $property_es->property_id = $v['property_id'];
                    }

                    $this->savePropertyData($property_es, $v);
                }

            }

            //eliminar las propiedades con 1 hora de antiguedad
            //Property::where('updated_at', '<=', date('Y-m-d H:i:s', strtotime('now -7 hours') ) )->delete();
            // LO COMENTE POR AHORA PARA QUE EL CRON NO BORRE NADA QUE NO DEBE

        } catch (Exception $e) {
            return [$e->getMessage()];
        }

        return [count($request->properties)];
    }


    private function savePropertyData($property, $data){
        $property->permalink = $data['permalink'];
        $property->title = $data['title'];
        $property->description = (isset($data['description'])) ? $data['description'] : '';
        $property->images = $data['images'];
        $property->lat = (isset($data['lat'])) ? $data['lat'] : '';
        $property->lng = (isset($data['lng'])) ? $data['lng'] : '';
        $property->price = (isset($data['price'])) ? $data['price'] : '';
        $property->price_type = (isset($data['price_type'])) ? $data['price_type'] : '';
        $property->baths = (isset($data['baths'])) ? $data['baths'] : 0;
        $property->beds = (isset($data['beds'])) ? $data['beds'] : 0;
        $property->parking = (isset($data['parking'])) ? $data['parking'] : 0;
        $property->status_id = (isset($data['status_id'])) ? $data['status_id'] : null;
        
        $property->location_id = null;
        if(isset($data['location'])){
            $locationString = strtolower(str_replace('-', ' ',$data['location']));
            $location = Location::where('title', $locationString)->where('lang', $data['lang'])->first();
            if($location){
                $property->location_id = $location->id;
            }   
        }

        $property->type_id = null;
        if(isset($data['propertyType'])){
            $typeString = strtolower(str_replace('-', ' ',$data['propertyType']));
            switch ($typeString) {
                case 'houses':
                    $typeString = 'house';
                    break;
                case 'condos':
                    $typeString = 'condo';
                    break;
                default:
                    $typeString = $typeString;
                    break;
            }
            $type = Type::where('title', $typeString)->where('lang', $data['lang'])->first();
            if($type){
                $property->type_id = $type->id;
            }   
        }

        //agregué este por que si no hay ningún cambio en las propiedades, la fecha de "updated_at" no cambia
        //por lo tanto se mantiene una fecha mayor a las 7 horas definidas y aún así se borran las propiedades.
        //entonces dar la fecha de hoy en el campo manual de "last_update" evita eso
        $property->last_update = date('Y-m-d H:i:s', strtotime('now'));

        if( $property->save() ){
            $features = array();
            if(isset($data['features']) && is_array($data['features']) && count($data['features'])>0 ){
                foreach($data['features'] as $feature){
                    $featureString = strtolower(str_replace('-', ' ', $feature));
                    $featureSearch = Feature::where('title', $featureString)->where('lang', $data['lang'])->first();
                    if($featureSearch){
                        $features[] = $featureSearch->id;
                    }
                }

                $property->features()->sync( $features );
            }
        }
    }

}