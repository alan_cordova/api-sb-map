<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Status;
use App\Type;

class SearchController extends Controller
{

    public function search(Request $request){

        $query = Property::where('lang', $request->lang);

        if($request->status){
            $query->where('status_id', $request->status);
        }

        if($request->type){
            $query->where('type_id', $request->type);
        }

        if($request->location){
            $query->where('location_id', $request->location);
        }

        if($request->beds){
            $query->where('beds', $request->beds);
        }

        if($request->baths){
            $query->where('baths', $request->baths);
        }

        if($request->min_price){
            $query->where('price', '>=', $request->min_price);
        }

        if($request->max_price){
            $query->where('price', '<=', $request->max_price);
        }

        if($request->search){
            $search = trim($request->search);
            if($search != ''){
                $query->Where(function($q) use($search){
                    $q->where('description', "like", "%". $search. "%" );
                    $q->orWhere('title', "like", "%". $search. "%" );
                });
            }
        }

        return $query->get();
    }

    public function prepareFields(Request $request){
        $res = [];

        $res['property_statuses'] = Status::where('lang', $request->lang)->orderBy('ordering', 'asc')->get();
        $res['property_types'] = Type::where('lang', $request->lang)->orderBy('ordering', 'asc')->get();

        //hacer un loop de todos y buscar si existen propiedades de cada uno, si no removerelos del return

        return $res;
    }

}