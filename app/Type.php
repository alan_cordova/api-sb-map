<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "types";
    protected $fillable = [ 'title', 'lang'];
    public $timestamps = false;

    public function properties(){
        return $this->hasMany('App\Property');
    }
}
