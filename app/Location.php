<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = "locations";
    protected $fillable = [ 'title', 'lang'];
    public $timestamps = false;

    public function properties(){
        return $this->hasMany('App\Property');
    }
}
